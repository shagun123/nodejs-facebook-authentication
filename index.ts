import * as express from 'express';
import * as  passport from "passport";
import {Strategy} from "passport-facebook";


const app = express();
app.listen(5000, () => {
    console.log('server is started');
})

app.use(passport.initialize());
app.use(passport.session());


passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

passport.use(new Strategy({
        clientID: '343119313226188',
        clientSecret: 'ffc667ae06c3d05a396410c7d8945b90',
        callbackURL: 'http://localhost:5000/fb/auth',
        profileFields: ['id', 'displayName']
    },
    function (accessToken, refreshToken, profile, done) {
        // if user exist by id
        // else user ko save krna hai
        const user = {};
        done(null, user);
    }
))


app.use('/login/fb', passport.authenticate('facebook'));


app.use('/failed/login', (req, res, next) => {
    res.send('login failed');
});
app.use('/fb/auth', passport.authenticate('facebook',
    {failureRedirect: '/failed/login'}), function (req: any, res) {
    res.send('logged in to facebook');
});

app.use('/logout', (req: any, res, next) => {
    req.logout();
    console.log(req.isAuthenticated());
    res.send('user is logged out');
})


